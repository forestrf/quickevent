Note: As is the case with most assets that require serialization, there is a chance that your event settings in the inspector could be lost or corrupted when updating the package or simply due to a bug in the code.

---- QuickEvent v1.0.1 ----

QuickEvents are used just like UnityEvents (http://docs.unity3d.com/Manual/UnityEvents.html) e.g.:

	public QuickEvent OnClickEvent;
	
	OnClickEvent.AddListener(() => Debug.Log("Non persistent listener"));
	int eventCount = OnClickEvent.GetPersistentEventCount();
	OnClickEvent.Invoke();

Use the AdvancedEvent class when you need multiple arguments support in the inspector e.g.:
	
	[System.Serializable] public class AdvancedStringEvent : AdvancedEvent<string> {}
	
	public AdvancedStringEvent OnSomeEvent;
	
	// Invoke event with default argument
	OnSomeEvent.Invoke("Hello, World!");


---- QuickEvent v1.0.2 ----

Fixes:

	• AdvancedEventEditor class was using QuickSupportedTypes instead of AdvancedSupportedTypes enum


---- QuickEvent v1.0.3 ----

Changes:

	• Updated variable names and code style to closer match C# conventions

