﻿using UnityEngine;
using UnityEditor;
using UnityEditorInternal;
using System;
using System.Collections.Generic;
using System.Reflection;
using System.CodeDom;
using System.Linq;
using Microsoft.CSharp;
using ByteSheep.Events;
using Object = UnityEngine.Object;

[CustomPropertyDrawer(typeof(QuickEventBase), true)]
public class QuickEventEditor : PropertyDrawer
{
    private const float IndentValue = 15f;
    private const float ToggleWidth = 16f;
    private const float ControlHeight = 16f;
    private bool inspectorDataIsNotNull = false;
    private ReorderableList reorderableList = null;
    private InspectorData inspectorData;

    [Serializable]
    public class InspectorData
    {
        public Type eventType;
        public SerializedProperty property;
        public SerializedProperty listProperty;
        public Rect position;
        public int indentLevel;
        public GUIContent label;
        public string eventHeaderText;
        public GenericMenu.MenuFunction2 menuSelectionHandler;
        public GenericMenu.MenuFunction2 noFunctionHandler;
        public PersistentCallData[] persistentCallData;

        public InspectorData(SerializedProperty property, GUIContent label, Rect position, int indentLevel, GenericMenu.MenuFunction2 menuSelectionHandler, GenericMenu.MenuFunction2 noFunctionHandler, FieldInfo fieldInfo)
        {
            this.property = property;
            this.listProperty = property.FindPropertyRelative("persistentCalls").FindPropertyRelative("calls");

            this.position = position;
            this.indentLevel = indentLevel;
            this.label = label;
            this.menuSelectionHandler = menuSelectionHandler;
            this.noFunctionHandler = noFunctionHandler;

            Type[] genericArguments = fieldInfo.FieldType.BaseType.GetGenericArguments();
            if (genericArguments.Length == 1)
                eventType = genericArguments[0];

            eventHeaderText = label.text + " (";
            for (int i = 0; i < genericArguments.Length; i++)
            {
                eventHeaderText += GetCleanTypeName(genericArguments[i]);
                if (i < genericArguments.Length - 1)
                    eventHeaderText += ", ";
            }
            eventHeaderText += ")";

            persistentCallData = new PersistentCallData[listProperty.arraySize];
            for (int i = 0; i < listProperty.arraySize; i++)
                persistentCallData[i] = new PersistentCallData(this, i, indentLevel, position, property, listProperty, menuSelectionHandler, noFunctionHandler);
        }

        public void UpdateDataValues(int index)
        {
            // Update PersistentCallData values
            persistentCallData[index] = new PersistentCallData(this, index, indentLevel, position, property, listProperty, menuSelectionHandler, noFunctionHandler);
        }
    }

    [Serializable]
    public class PersistentCallData
    {
        public InspectorData inspectorData;
        public SerializedProperty persistentCallProperty;
        public SerializedProperty genericMenuDataProperty;
        public SerializedProperty componentIndexProperty;
        public SerializedProperty memberIndexProperty;
        public SerializedProperty isDynamicDataProperty;

        public SerializedProperty targetProperty;
        public SerializedProperty memberNameProperty;
        public SerializedProperty memberTypeProperty;
        public SerializedProperty isDynamicProperty;
        public SerializedProperty isCallEnabledProperty;
        public SerializedProperty argumentTypeProperty;

        public GameObject targetGameObject;
        public int index;
        public GenericMenu genericMenu;
        public MemberMenuData genericMenuData;
        public string selectedOptionText;
        public string shortSelectedOptionText;

        public PersistentCallData(InspectorData inspectorData, int index, int indentLevel, Rect position, SerializedProperty property, SerializedProperty listProperty, GenericMenu.MenuFunction2 menuSelectionHandler, GenericMenu.MenuFunction2 noFunctionHandler)
        {
            this.inspectorData = inspectorData;
            this.index = index;
            this.persistentCallProperty = listProperty.GetArrayElementAtIndex(index);
            this.genericMenuDataProperty = persistentCallProperty.FindPropertyRelative("genericMenuData");
            this.componentIndexProperty = genericMenuDataProperty.FindPropertyRelative("selectedComponent");
            this.memberIndexProperty = genericMenuDataProperty.FindPropertyRelative("selectedMember");
            this.isDynamicDataProperty = genericMenuDataProperty.FindPropertyRelative("isDynamic");

            this.targetProperty = persistentCallProperty.FindPropertyRelative("target");
            this.memberNameProperty = persistentCallProperty.FindPropertyRelative("memberName");
            this.memberTypeProperty = persistentCallProperty.FindPropertyRelative("memberType");
            this.isDynamicProperty = persistentCallProperty.FindPropertyRelative("isDynamic");
            this.isCallEnabledProperty = persistentCallProperty.FindPropertyRelative("isCallEnabled");
            this.argumentTypeProperty = persistentCallProperty.FindPropertyRelative("argument").FindPropertyRelative("supportedType");

            // Try to cast assigned target to GameObject type
            targetGameObject = targetProperty.objectReferenceValue as GameObject;
            Component targetComponent = targetProperty.objectReferenceValue as Component;

            if (targetGameObject == null)
            {
                // If that doesn't work, perhaps a component was assigned
                if (targetComponent != null)
                    targetGameObject = targetComponent.gameObject;
                // If it wasn't a component or not an instance then set the field to null
                if (targetGameObject == null)
                    targetProperty.objectReferenceValue = null;
            }

            if (targetGameObject == null) return;

            // Add all components and the game object reference to the selectable list
            Component[] components = targetGameObject.GetComponents(typeof(Component));
            Object[] gameObjectAndComponents = new Object[components.Length + 1];
            gameObjectAndComponents[0] = targetGameObject;
            components.CopyTo(gameObjectAndComponents, 1);
            genericMenuData = new MemberMenuData(inspectorData, gameObjectAndComponents);

            // Check if components have been added or moved up/down in the inspector
            if (componentIndexProperty.intValue != -1)
                for (int i = 0; i < components.Length; i++)
                    if (components[i] == targetComponent)
                        componentIndexProperty.intValue = i + 1;

            genericMenu = new GenericMenu();
            bool isNoFunctionSelected = componentIndexProperty.intValue == -1;
            genericMenu.AddItem(new GUIContent("No Function"), isNoFunctionSelected, noFunctionHandler, new MemberMenuIndex(index, 0, 0, false));
            genericMenu.AddSeparator("");

            for (int i = 0; i < genericMenuData.components.Length; i++)
            {
                MemberMenuComponent component = genericMenuData.components[i];

                for (int j = 0; j < component.dynamicMembers.Length; j++)
                {
                    string prefix = component.displayName + "/";
                    if (j == 0)
                        genericMenu.AddDisabledItem(new GUIContent(prefix + "Dynamic " + GetCleanTypeName(inspectorData.eventType)));

                    bool isSelected = (isDynamicProperty.boolValue && i == componentIndexProperty.intValue && component.GetMemberItem(j, true).index == memberIndexProperty.intValue);
                    genericMenu.AddItem(new GUIContent(prefix + component.dynamicMembers[j].memberInfo.Name), isSelected, menuSelectionHandler, new MemberMenuIndex(index, i, j, true));
                }

                if (component.dynamicMembers.Length > 0)
                    genericMenu.AddDisabledItem(new GUIContent(component.displayName + "/ "));

                for (int j = 0; j < component.staticMembers.Length; j++)
                {
                    string prefix = component.displayName + "/";
                    if (j == 0)
                        genericMenu.AddDisabledItem(new GUIContent(prefix + "Static Parameters"));

                    bool isSelected = (!isDynamicProperty.boolValue && i == componentIndexProperty.intValue && component.GetMemberItem(j, false).index == memberIndexProperty.intValue);
                    genericMenu.AddItem(new GUIContent(prefix + component.staticMembers[j].menuDisplayName), isSelected, menuSelectionHandler, new MemberMenuIndex(index, i, j, false));
                }
            }

            selectedOptionText = "No Function";
            shortSelectedOptionText = selectedOptionText;
			
            if (componentIndexProperty.intValue >= 0)
            {
                MemberMenuComponent menuComponent = genericMenuData.components[Mathf.Clamp(componentIndexProperty.intValue, 0, genericMenuData.components.Length - 1)];
                MemberMenuItem menuItem = menuComponent.GetMemberItem(memberIndexProperty.intValue, isDynamicProperty.boolValue);
                if (menuItem != null)
                {
                    shortSelectedOptionText = menuItem.shortDisplayName;
                    selectedOptionText = menuComponent.component.GetType().Name + "." + shortSelectedOptionText;
                }
            }

            property.serializedObject.ApplyModifiedProperties();
        }
    }

    public class MemberMenuIndex
    {
        public int persistentCallIndex;
        public int componentIndex = -1;
        public int memberIndex;
        public bool isDynamic;

        public MemberMenuIndex(int persistentCallIndex, int componentIndex, int memberIndex, bool isDynamic)
        {
            this.persistentCallIndex = persistentCallIndex;
            this.componentIndex = componentIndex;
            this.memberIndex = memberIndex;
            this.isDynamic = isDynamic;
        }
    }

    public class MemberMenuData
    {
        public InspectorData inspectorData;
        public MemberMenuComponent[] components;
        private Dictionary<Type, int> componentOccurrences = new Dictionary<Type, int>();

        public MemberMenuData(InspectorData inspectorData, Object[] components)
        {
            this.inspectorData = inspectorData;
            this.components = new MemberMenuComponent[components.Length];
            for (int i = 0; i < components.Length; i++)
            {
                int occurences = 1;
                if (componentOccurrences.ContainsKey(components[i].GetType()))
                    occurences = ++componentOccurrences[components[i].GetType()];
                else
                    componentOccurrences.Add(components[i].GetType(), occurences);

                this.components[i] = new MemberMenuComponent(i, components[i], components[i].GetType().Name + (occurences > 1 ? (" " + (occurences - 1)) : ""), inspectorData.eventType);
            }
        }

        public MemberMenuComponent GetComponent(int index)
        {
            return components[Mathf.Clamp(index, 0, Mathf.Max(components.Length - 1))];
        }

        public MemberMenuItem GetMemberItem(int componentIndex, int index, bool isDynamic)
        {
            return components[Mathf.Clamp(componentIndex, 0, Mathf.Max(0, components.Length - 1))].GetMemberItem(index, isDynamic);
        }
    }

    public class MemberMenuComponent
    {
        public int index;
        public Object component;
        public string displayName;
        public Type eventType;
        public MemberMenuItem[] staticMembers;
        public MemberMenuItem[] dynamicMembers;

        public MemberMenuComponent(int index, Object component, string displayName, Type eventType)
        {
            this.index = index;
            this.component = component;
            this.displayName = displayName;
            this.eventType = eventType;
            // Automatically create members list when new instance of this class is created
            CreateStaticMemberItems(GetValidStaticMembers());
            CreateDynamicMemberItems(GetValidDynamicMembers());
        }

        public MemberInfo[] GetValidDynamicMembers()
        {
            // Sort out all the valid members
            MemberInfo[] members = component.GetType().GetMembers(BindingFlags.Instance | BindingFlags.Public);
            List<MemberInfo> validDynamicMembers = new List<MemberInfo>();
            List<MemberInfo> validDynamicMethods = new List<MemberInfo>();

            Type eventType = this.eventType;
            if (eventType == null) return new MemberInfo[0];

            for (int i = 0; i < members.Length; i++)
            {
                if (members[i].MemberType == MemberTypes.Property)
                {
                    PropertyInfo propertyInfo = members[i] as PropertyInfo;
                    if (propertyInfo.PropertyType == eventType &&
                        propertyInfo.GetGetMethod(false) != null &&
                        propertyInfo.GetSetMethod(false) != null &&
                        propertyInfo.CanWrite &&
                        !propertyInfo.IsDefined(typeof(ObsoleteAttribute), true))
                    {
                        validDynamicMembers.Add(propertyInfo);
                    }
                }
                else if (members[i].MemberType == MemberTypes.Field)
                {
                    FieldInfo fieldInfo = members[i] as FieldInfo;
                    if (fieldInfo.FieldType == eventType && fieldInfo.IsInitOnly && !fieldInfo.IsDefined(typeof(ObsoleteAttribute), true))
                    {
                        validDynamicMembers.Add(fieldInfo);
                    }
                }
            }
            // Next list all methods
            for (int i = 0; i < members.Length; i++)
            {
                if (members[i].MemberType == MemberTypes.Method)
                {
                    MethodInfo methodInfo = members[i] as MethodInfo;
                    ParameterInfo[] parameters = methodInfo.GetParameters();
                    if (!methodInfo.IsSpecialName &&
                        methodInfo.ReturnType == typeof(void) &&
                        parameters.Length == 1 &&
                        parameters[0].ParameterType == eventType &&
                        !methodInfo.IsDefined(typeof(ObsoleteAttribute), true))
                    {
                        validDynamicMethods.Add(methodInfo);
                    }
                }
            }

            // Sort members alphabetically
            validDynamicMembers = validDynamicMembers.OrderBy(x => x.Name).ToList();
            validDynamicMethods = validDynamicMethods.OrderBy(x => x.Name).ToList();

            return validDynamicMembers.Concat(validDynamicMethods).ToArray();
        }

        public bool ArrayContains<T>(T[] array, T value) where T : class
        {
            for (int i = 0; i < array.Length; i++)
                if (array[i] == value)
                    return true;
            return false;
        }

        public static bool IsSameOrSubclass(Type potentialBase, Type potentialDescendant)
        {
            return potentialDescendant.IsSubclassOf(potentialBase) || potentialDescendant == potentialBase;
        }

        public MemberInfo[] GetValidStaticMembers()
        {
            // Sort out all the valid members
            MemberInfo[] members = component.GetType().GetMembers(BindingFlags.Instance | BindingFlags.Public);
            List<MemberInfo> validMembers = new List<MemberInfo>();
            List<MemberInfo> validMethods = new List<MemberInfo>();
            Type[] supportedTypes = new Type[] { typeof(string), typeof(int), typeof(float), typeof(bool), typeof(Color), typeof(Vector2), typeof(Vector3), typeof(Object), typeof(GameObject), typeof(Transform) };

            // List all properties and fields first
            for (int i = 0; i < members.Length; i++)
            {
                if (members[i].MemberType == MemberTypes.Property)
                {
                    PropertyInfo propertyInfo = members[i] as PropertyInfo;
                    if (!propertyInfo.IsDefined(typeof(ObsoleteAttribute), true) && propertyInfo.CanWrite && ArrayContains(supportedTypes, propertyInfo.PropertyType))
                        validMembers.Add(propertyInfo);
                }
                else if (members[i].MemberType == MemberTypes.Field)
                {
                    FieldInfo fieldInfo = members[i] as FieldInfo;
                    if (!fieldInfo.IsDefined(typeof(ObsoleteAttribute), true) && ArrayContains(supportedTypes, fieldInfo.FieldType))
                        validMembers.Add(fieldInfo);
                }
            }
            // Next list all methods
            for (int i = 0; i < members.Length; i++)
            {
                if (members[i].MemberType == MemberTypes.Method)
                {
                    MethodInfo methodInfo = members[i] as MethodInfo;
                    if (methodInfo.ReturnType == typeof(void) && !methodInfo.IsSpecialName && !methodInfo.IsDefined(typeof(ObsoleteAttribute), true))
                    {
                        ParameterInfo[] parameters = methodInfo.GetParameters();
                        if ((parameters.Length == 0) || (parameters.Length == 1 && ArrayContains(supportedTypes, parameters[0].ParameterType)))
                            validMethods.Add(methodInfo);
                    }
                }
            }

            // Sort members alphabetically
            validMembers = validMembers.OrderBy(x => x.Name).ToList();
            validMethods = validMethods.OrderBy(x => x.Name).ToList();

            return validMembers.Concat(validMethods).ToArray();
        }

        public void CreateDynamicMemberItems(MemberInfo[] membersInfo)
        {
            dynamicMembers = new MemberMenuItem[membersInfo.Length];
            for (int i = 0; i < membersInfo.Length; i++)
                dynamicMembers[i] = new MemberMenuItem(i, true, membersInfo[i]);
        }

        public void CreateStaticMemberItems(MemberInfo[] membersInfo)
        {
            staticMembers = new MemberMenuItem[membersInfo.Length];
            for (int i = 0; i < membersInfo.Length; i++)
                staticMembers[i] = new MemberMenuItem(i, false, membersInfo[i]);
        }

        public MemberMenuItem GetMemberItem(int index, bool isDynamic)
        {
            if (isDynamic && dynamicMembers.Length > 0)
                return dynamicMembers[Mathf.Clamp(index, 0, dynamicMembers.Length - 1)];
            else if (!isDynamic && staticMembers.Length > 0)
                return staticMembers[Mathf.Clamp(index, 0, staticMembers.Length - 1)];
            else
                return null;
        }
    }

    public class MemberMenuItem
    {
        public int index;
        public bool isDynamic;
        public string menuDisplayName;
        public string shortDisplayName;
        public MemberInfo memberInfo;

        public MemberMenuItem(int index, bool isDynamic, MemberInfo memberInfo, string menuDisplayName = "", string shortDisplayName = "")
        {
            this.index = index;
            this.isDynamic = isDynamic;
            this.memberInfo = memberInfo;

            if (menuDisplayName == String.Empty)
                this.menuDisplayName = GetDisplayName(memberInfo, true);
            else
                this.menuDisplayName = menuDisplayName;

            if (shortDisplayName == String.Empty)
                this.shortDisplayName = GetDisplayName(memberInfo, false);
            else
                this.shortDisplayName = shortDisplayName;
        }

        public static string GetDisplayName(MemberInfo memberInfo, bool prependMemberType)
        {
            if (memberInfo.MemberType == MemberTypes.Method)
            {
                ParameterInfo[] parameters = (memberInfo as MethodInfo).GetParameters();
                return memberInfo.Name + " (" + ((parameters.Length > 0) ? QuickEventEditor.GetCleanTypeName(parameters[0].ParameterType) : "") + ")";
            }
            else if (memberInfo.MemberType == MemberTypes.Property)
            {
                PropertyInfo propertyInfo = memberInfo as PropertyInfo;
                return (prependMemberType ? QuickEventEditor.GetCleanTypeName(propertyInfo.PropertyType) + " " : "") + propertyInfo.Name;
            }
            else
            {
                FieldInfo fieldInfo = memberInfo as FieldInfo;
                return (prependMemberType ? QuickEventEditor.GetCleanTypeName(fieldInfo.FieldType) + " " : "") + fieldInfo.Name;
            }
        }
    }

    private SerializedProperty GetList(SerializedProperty property = null)
    {
        if (inspectorData.listProperty == null)
        {
            if (property == null && inspectorData.property == null)
                return null;

            return inspectorData.listProperty = (property ?? inspectorData.property).FindPropertyRelative("persistentCalls").FindPropertyRelative("calls");
        }
        else
            return inspectorData.listProperty;
    }

    private ReorderableList GetReorderableList(SerializedProperty property, GUIContent label)
    {
        if (reorderableList == null)
        {
            reorderableList = new ReorderableList(property.serializedObject, property, true, true, true, true);
            reorderableList.drawElementCallback = DrawElement;

            reorderableList.onRemoveCallback = (ReorderableList list) =>
            {
                ReorderableList.defaultBehaviours.DoRemoveButton(list);
                // Calculate and save new list height
                inspectorData.property.FindPropertyRelative("inspectorListHeight").floatValue = Mathf.Max(1, list.count) * list.elementHeight + 38f;
                // Signal that inspector data needs updating
                inspectorDataIsNotNull = false;
            };

            reorderableList.onAddCallback = (ReorderableList list) =>
            {
                ReorderableList.defaultBehaviours.DoAddButton(list);
                // Calculate and save new list height
                inspectorData.property.FindPropertyRelative("inspectorListHeight").floatValue = Mathf.Max(1, list.count) * list.elementHeight + 38f;
                // if this isn't a duplicated list element with preassigned values then set defaults
                if (property.GetArrayElementAtIndex(property.arraySize - 1).FindPropertyRelative("target").objectReferenceValue == null)
                {
                    property.GetArrayElementAtIndex(property.arraySize - 1).FindPropertyRelative("genericMenuData").FindPropertyRelative("selectedComponent").intValue = -1;
                    property.GetArrayElementAtIndex(property.arraySize - 1).FindPropertyRelative("isCallEnabled").boolValue = true;
                }
                // Signal that inspector data needs updating
                inspectorDataIsNotNull = false;
            };

            reorderableList.onChangedCallback = (ReorderableList list) =>
            {
                // Signal that inspector data needs updating
                inspectorDataIsNotNull = false;
            };
            // TODO: Only generate header text once
            reorderableList.drawHeaderCallback = (Rect rect) =>
            {
                string genericArgumentsText = "";
                Type[] genericArguments = fieldInfo.FieldType.BaseType.GetGenericArguments();
                for (int i = 0; i < genericArguments.Length; i++)
                {
                    genericArgumentsText += GetCleanTypeName(genericArguments[i]);
                    if (i < genericArguments.Length - 1)
                        genericArgumentsText += ", ";
                }
                // Draw event header
                EditorGUI.LabelField(rect, label.text + " (" + genericArgumentsText + ")");
            };
        }
        return reorderableList;
    }

    public void GenericMenuSelectionHandler(object menuIndexInfo)
    {
        MemberMenuIndex menuIndex = menuIndexInfo as MemberMenuIndex;
        PersistentCallData persistentCall = inspectorData.persistentCallData[menuIndex.persistentCallIndex];

        if (menuIndex == null || persistentCall.targetProperty.objectReferenceValue == null) return;

        persistentCall.componentIndexProperty.intValue = menuIndex.componentIndex;
        persistentCall.memberIndexProperty.intValue = menuIndex.memberIndex;
        persistentCall.isDynamicDataProperty.boolValue = menuIndex.isDynamic;
        persistentCall.isDynamicProperty.boolValue = menuIndex.isDynamic;

        // Get the info of the option we selected in the generic menu
        MemberMenuItem menuItem = persistentCall.genericMenuData.GetComponent(menuIndex.componentIndex).GetMemberItem(menuIndex.memberIndex, menuIndex.isDynamic);
        // Set target object to reference of component selected in generic dropdown menu
        persistentCall.targetProperty.objectReferenceValue = (menuIndex.componentIndex < 0) ? persistentCall.targetGameObject : persistentCall.genericMenuData.GetComponent(menuIndex.componentIndex).component;

        if (menuItem == null) return;

        // Set target object to reference of component selected in generic dropdown menu
        persistentCall.targetProperty.objectReferenceValue = persistentCall.genericMenuData.GetComponent(menuIndex.componentIndex).component;
        // Set memberName to string at selectedIndex
        persistentCall.memberNameProperty.stringValue = menuItem.memberInfo.Name;
        // Set memberType enum value
        persistentCall.memberTypeProperty.enumValueIndex = GetMemberTypeIndex(menuItem.memberInfo.MemberType);

        // Set the function argument type
        Type argumentType = typeof(void);
        if (menuItem.memberInfo.MemberType == MemberTypes.Method)
        {
            ParameterInfo[] parameterInfo = (menuItem.memberInfo as MethodInfo).GetParameters();
            argumentType = parameterInfo.Length == 0 ? typeof(void) : parameterInfo[0].ParameterType;
        }
        else if (menuItem.memberInfo.MemberType == MemberTypes.Property)
            argumentType = (menuItem.memberInfo as PropertyInfo).PropertyType;
        else if (menuItem.memberInfo.MemberType == MemberTypes.Field)
            argumentType = (menuItem.memberInfo as FieldInfo).FieldType;

        persistentCall.argumentTypeProperty.enumValueIndex = (int)GetArgumentType(argumentType);
        inspectorData.UpdateDataValues(menuIndex.persistentCallIndex);
        inspectorData.property.serializedObject.ApplyModifiedProperties();

        GUIUtility.hotControl = 0;
    }

    public void NoFunctionHandler(object menuIndexInfo)
    {
        MemberMenuIndex menuIndex = menuIndexInfo as MemberMenuIndex;
        if (menuIndex == null) return;

        // Clear serialized member name and type info
        PersistentCallData persistentCall = inspectorData.persistentCallData[menuIndex.persistentCallIndex];
        persistentCall.componentIndexProperty.intValue = -1;
        persistentCall.memberNameProperty.stringValue = "";
        persistentCall.memberTypeProperty.enumValueIndex = GetMemberTypeIndex(MemberTypes.Method);
        persistentCall.argumentTypeProperty.enumValueIndex = (int)QuickSupportedTypes.Void;

        inspectorData.UpdateDataValues(menuIndex.persistentCallIndex);
        inspectorData.property.serializedObject.ApplyModifiedProperties();

        GUIUtility.hotControl = 0;
    }

    private void DrawElement(Rect position, int index, bool isActive, bool isFocused)
    {
        if (inspectorData == null || inspectorData.persistentCallData == null)
            return;

        // Calculate control positions
        int indentLevel = EditorGUI.indentLevel;
        int divisions = 3;
        float top = position.y + 4f;
        float left = position.x + ToggleWidth - indentLevel * IndentValue;
        float fullWidth = position.width - ToggleWidth;

        Rect dynamicSize = new Rect(position.x - indentLevel * IndentValue, top, fullWidth, ControlHeight);
        float itemWidth = dynamicSize.width / divisions;

        Rect controlRect0 = new Rect(left - ToggleWidth, top, ToggleWidth, ControlHeight);
        Rect controlRect1 = new Rect(left + 0f * itemWidth + 1f, top, itemWidth + indentLevel * IndentValue - 1f, ControlHeight);
        Rect controlRect2 = new Rect(left + 1f * itemWidth + 1f, top, itemWidth + indentLevel * IndentValue - 1f, ControlHeight);
        Rect controlRect3 = new Rect(left + 2f * itemWidth + 2f, top, itemWidth + indentLevel * IndentValue - 1f, ControlHeight);

        PersistentCallData persistentCall = inspectorData.persistentCallData[index];
        EditorGUI.PropertyField(controlRect0, persistentCall.isCallEnabledProperty, GUIContent.none);
        EditorGUI.BeginChangeCheck();
        EditorGUI.PropertyField(controlRect1, persistentCall.targetProperty, GUIContent.none);
        if (EditorGUI.EndChangeCheck())
        {
            // If the target object field has changed, update the inspector data and the dropdown selection
            inspectorData = new InspectorData(inspectorData.property, inspectorData.label, position, inspectorData.indentLevel, GenericMenuSelectionHandler, NoFunctionHandler, fieldInfo);
            persistentCall = inspectorData.persistentCallData[index];
            GenericMenuSelectionHandler(new MemberMenuIndex(index, persistentCall.componentIndexProperty.intValue, persistentCall.memberIndexProperty.intValue, persistentCall.isDynamicDataProperty.boolValue));
        }

        if (persistentCall.targetProperty.objectReferenceValue == null)
            GUI.enabled = false;
        else if (Event.current != null && Event.current.type == EventType.MouseDown && controlRect2.Contains(Event.current.mousePosition))
            persistentCall.genericMenu.DropDown(new Rect(controlRect2.x, controlRect2.y + 9f, 0f, 0f));

        GUIStyle popupStyle = new GUIStyle(EditorStyles.popup);
        popupStyle.fixedHeight = ControlHeight;
        EditorGUI.LabelField(controlRect2, new GUIContent(persistentCall.shortSelectedOptionText, persistentCall.selectedOptionText), popupStyle);
        GUI.enabled = true;

        if (persistentCall.targetProperty.objectReferenceValue != null && persistentCall.componentIndexProperty.intValue != -1 && !persistentCall.isDynamicProperty.boolValue)
            DrawArgumentField(controlRect3, index);
    }

    private void DrawArgumentField(Rect position, int index)
    {
        // Based on the method/property/field selected in the dropdown, show the corresponding argument field.
        SerializedProperty persistentCallProperty = GetList(inspectorData.property).GetArrayElementAtIndex(index);
        SerializedProperty argumentProperty = persistentCallProperty.FindPropertyRelative("argument");
        SerializedProperty argumentTypeProperty = argumentProperty.FindPropertyRelative("supportedType");

        SerializedProperty stringArgumentProperty = argumentProperty.FindPropertyRelative("stringArgument");
        SerializedProperty intArgumentProperty = argumentProperty.FindPropertyRelative("intArgument");
        SerializedProperty floatArgumentProperty = argumentProperty.FindPropertyRelative("floatArgument");
        SerializedProperty boolArgumentProperty = argumentProperty.FindPropertyRelative("boolArgument");
        SerializedProperty colorArgumentProperty = argumentProperty.FindPropertyRelative("colorArgument");
        SerializedProperty vector2ArgumentProperty = argumentProperty.FindPropertyRelative("vector2Argument");
        SerializedProperty vector3ArgumentProperty = argumentProperty.FindPropertyRelative("vector3Argument");
        SerializedProperty objectArgumentProperty = argumentProperty.FindPropertyRelative("objectArgument");
        SerializedProperty gameObjectArgumentProperty = argumentProperty.FindPropertyRelative("gameObjectArgument");
        SerializedProperty transformArgumentProperty = argumentProperty.FindPropertyRelative("transformArgument");

        SerializedProperty property = objectArgumentProperty;

        if (argumentTypeProperty.enumValueIndex == (int)QuickSupportedTypes.String)
            property = stringArgumentProperty;
        else if (argumentTypeProperty.enumValueIndex == (int)QuickSupportedTypes.Int)
            property = intArgumentProperty;
        else if (argumentTypeProperty.enumValueIndex == (int)QuickSupportedTypes.Float)
            property = floatArgumentProperty;
        else if (argumentTypeProperty.enumValueIndex == (int)QuickSupportedTypes.Bool)
            property = boolArgumentProperty;
        else if (argumentTypeProperty.enumValueIndex == (int)QuickSupportedTypes.Color)
            property = colorArgumentProperty;
        else if (argumentTypeProperty.enumValueIndex == (int)QuickSupportedTypes.Vector2)
            property = vector2ArgumentProperty;
        else if (argumentTypeProperty.enumValueIndex == (int)QuickSupportedTypes.Vector3)
            property = vector3ArgumentProperty;
        else if (argumentTypeProperty.enumValueIndex == (int)QuickSupportedTypes.Object)
            property = objectArgumentProperty;
        else if (argumentTypeProperty.enumValueIndex == (int)QuickSupportedTypes.GameObject)
            property = gameObjectArgumentProperty;
        else if (argumentTypeProperty.enumValueIndex == (int)QuickSupportedTypes.Transform)
            property = transformArgumentProperty;

        if (argumentTypeProperty.enumValueIndex != (int)QuickSupportedTypes.Void)
            EditorGUI.PropertyField(position, property, GUIContent.none);
    }

    public override void OnGUI(Rect position, SerializedProperty property, GUIContent label)
    {
        int indent = EditorGUI.indentLevel;
        position = new Rect(position.x + indent * IndentValue, position.y, position.width - indent * IndentValue, position.height);

        var listProperty = property.FindPropertyRelative("persistentCalls").FindPropertyRelative("calls");
        var list = GetReorderableList(listProperty, label);

        if (!inspectorDataIsNotNull)
        {
            inspectorData = new InspectorData(property, label, position, indent, GenericMenuSelectionHandler, NoFunctionHandler, fieldInfo);
            inspectorDataIsNotNull = true;
        }

        list.elementHeight = 25f;
        property.FindPropertyRelative("inspectorListHeight").floatValue = Mathf.Max(1, list.count) * list.elementHeight + 38f;
        list.DoList(position);

        EditorGUI.indentLevel = indent;
    }

    public static string GetCleanTypeName(Type type)
    {
        string typeName;
        using (var provider = new CSharpCodeProvider())
        {
            var typeRef = new CodeTypeReference(type);
            typeName = provider.GetTypeOutput(typeRef);
            string[] nameSpaceName = typeName.Split('.');
            typeName = nameSpaceName[nameSpaceName.Length - 1];
        }
        return typeName;
    }

    public override float GetPropertyHeight(SerializedProperty property, GUIContent label)
    {
        return property.FindPropertyRelative("inspectorListHeight").floatValue;
    }

    public QuickSupportedTypes GetArgumentType(Type type)
    {
        if (type == typeof(string))
            return QuickSupportedTypes.String;
        else if (type == typeof(int))
            return QuickSupportedTypes.Int;
        else if (type == typeof(float))
            return QuickSupportedTypes.Float;
        else if (type == typeof(bool))
            return QuickSupportedTypes.Bool;
        else if (type == typeof(Color))
            return QuickSupportedTypes.Color;
        else if (type == typeof(Vector2))
            return QuickSupportedTypes.Vector2;
        else if (type == typeof(Vector3))
            return QuickSupportedTypes.Vector3;
        else if (type == typeof(Object)) //(MemberMenuComponent.IsSameOrSubclass (typeof (Object), type))
            return QuickSupportedTypes.Object;
        else if (type == typeof(GameObject))
            return QuickSupportedTypes.GameObject;
        else if (type == typeof(Transform))
            return QuickSupportedTypes.Transform;
        else
            return QuickSupportedTypes.Void;
    }

    // For some reason unity's dropdown maps the indices to different enum values
    private int GetMemberTypeIndex(MemberTypes type)
    {
        switch (type)
        {
            case MemberTypes.Property:
                return 4;
            case MemberTypes.Field:
                return 2;
            default:
                return 3;
        }
    }
}
